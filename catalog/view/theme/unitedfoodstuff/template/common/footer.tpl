<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <ul class="list-unstyled">
          <li>Blk 3017 #06-11</li>
          <li>Bedok North Street 5, Gourmet East Kitchen,</li>
          <li>Singapore 486121</li>
        </ul>
      </div>
      <div class="col-sm-4">
        <ul class="list-unstyled">
          <li>Tel: 6243 8958</li>
          <li> Fax: 6243 8956</li>
        </ul>
      </div>
      <div class="col-sm-4">
        <ul class="list-unstyled">
          <li>&copy;2014 United Foodstuff Co. Pte Ltd. All rights reserved.</li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>